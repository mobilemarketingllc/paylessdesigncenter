<?php

class WP_Example_Process {

	use WP_Example_Logger;

	/**
	 * @var string
	 */
	protected $action = 'example_process';

	/**
	 * Task
	 *
	 * Override this method to perform any actions required on each
	 * queue item. Return the modified item for further processing
	 * in the next pass through. Or, return false to remove the
	 * item from the queue.
	 *
	 * @param mixed $item Queue item to iterate over
	 *
	 * @return mixed
	 */
	protected function task( $item,$main_category ) {

		if(!is_array($item) || count($item)==0){
			return false;
		}
      
	for($i=0;$i<count($item);$i++)
		$post_id = $this->insert_product($item[$i],$main_category);

		//$message = $this->get_message( $item['name'],$post_id );

		//$this->really_long_running_task();
		//$this->log( $message );

		return false;
	}

	/**
	 * Complete
	 *
	 * Override if applicable, but ensure that the below actions are
	 * performed, or, call parent::complete().
	 */
	protected function complete() {
		parent::complete();

		// Show notice to user or perform some other arbitrary task...
	}

}